#include <stdio.h>
#include <stdlib.h>

int main(void) {
	
	int *iptr = NULL;
	iptr = (int *)malloc(sizeof(int));

	if(iptr == NULL) {

		fprintf(stderr, "Error in allocation of memory");
		exit(EXIT_FAILURE);
	}

	*iptr = 100;
	printf("The value in assigned to the dynamically allocated integer is %d\n", *iptr);

	free(iptr);
	iptr = NULL;

	exit(EXIT_SUCCESS);
}

